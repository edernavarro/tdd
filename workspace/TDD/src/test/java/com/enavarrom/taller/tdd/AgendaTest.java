package com.enavarrom.taller.tdd;

import org.junit.Assert;
import org.junit.Test;

public class AgendaTest {
	
	@SuppressWarnings("deprecation")
	@Test
	public void whenAddNewContactThenSave() {
		
		//Given
		Contacto contacto = new Contacto();
		contacto.setNombre("RAFAEL");
		contacto.setApellido("MARTINEZ");
		contacto.setTelefono("12345");
		
		Contacto contacto2 = new Contacto();
		contacto2.setNombre("RAFAEL2");
		contacto2.setApellido("MARTINEZ2");
		contacto2.setTelefono("123456");
		
		//When
		Agenda agenda = new Agenda();
		agenda.addContacto(contacto);
		agenda.addContacto(contacto2);
		
		//Then
		Assert.assertTrue(agenda.getContactoByTelefono("12345").isPresent());
		Assert.assertEquals(contacto, agenda.getContactoByTelefono("12345").get());
	}
	
	public void whenAddNewContactThenFail() {
		
	}

}
