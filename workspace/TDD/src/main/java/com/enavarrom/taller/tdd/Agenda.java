package com.enavarrom.taller.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Agenda {
	
	private List<Contacto> contactos;
	
	public Agenda() {
		contactos = new ArrayList<>();
	}

	public void addContacto(Contacto contacto) {
		contactos.add(contacto);
	}

	public Optional<Contacto> getContactoByTelefono(String telefono) {
		return contactos.stream()
		.filter(contacto -> telefono.equals(contacto.getTelefono()))
		.findFirst();
	}

}
